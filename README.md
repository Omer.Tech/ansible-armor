<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/Omer.Tech/ansible-armor">
    <img src="./.images/logo_readme.png" alt="Logo">
  </a>

  <h3 align="center">Ansible Armor</h3>

  <p align="center">
    Automate your Linux hardening
    <br />
    <br />
    <a href="https://gitlab.com/Omer.Tech/ansible-armor/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Omer.Tech/ansible-armor/issues">Request Feature</a>
    ·
    <a href="https://gitlab.com/Omer.Tech/ansible-armor/-/merge_requests">Send a Pull Request</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project  
I originally started this project to automate some Linux hardening tasks for my school's  
Cybersecurity team. We have started using this script during our blue-team competitions to  
help free up our Linux administrators to help with other tasks. I first created this concept as a bash script called [Linux Lockdown](https://gitlab.com/Omer.Tech/LinuxLockdown). I wanted to recompile this script as an ansible playbook in order to furter streamline the process of hardening Linux systems. The bash script is a great option if you want more versatility and only have to secure a couple of systems. If you need to secure a larger network of Linux systems, I recommend this playbook instead. I currently have logic in this playbook to handle Linux distros that are either Debian or Redhat forks. So distros like Ubuntu, CentOS, Fedora, etc. should all also be able to run this.  

This Absible playbook will:  
* Update and upgrade all packages currently installed on the system.  
* Install the EPEL Repository on any RedHat based systems.  
* Install Uncomplicated Firewall (ufw), fail2ban, and logwatch.  
* Install Very Secure File Transfer Protocol (VSFTP).  
* Import and apply a hardened VSFTP configuration.  
* Import and apply a hardened SSH configuration.  
* Configure firewall to allow SSH and HTTP through by default.  
* Configure firewall to drop all other connections.  
  
Of course, this script will not protect against all threats, and will not completely secure your machine. This script should be used as a tool to help automate some simple security tasks, and should not be seen as an all-in-one solution for your Linux security needs.  

A list of commonly used resources that I find helpful are listed in the acknowledgements.  

### Built With  
This playbook was all written in .yml using Ansible's standard configuration. I implemented testing of this playbook using Vagrant. 
I have left my Vagrantfile in the repository in case anyone wants to test their own modifications to the playbook.  
  
To test this playbook in your own environment, simply install some form of Virtualization software (Virtualbox, VMWare, etc.) and Vagrant, navigate to this directory using a terminal, and run the command 'vagrant up' this will download and build an Ubuntu-Master machine, an Ubuntu-Target machine, and a Centos-Target machine.  
Once those are built, you'll want to run 'vagrant ssh Ubuntu-Master' this will open a Secure Shell on the Ubuntu-Master VM.  
Finally, you can cd to '/vagrant' and find this repository mounted there. You'll be able to run the playbook and test any changes.  
* [Ansible](https://www.ansible.com/)  
* [Vagrant](https://www.vagrantup.com/)
  
<!-- GETTING STARTED -->
## Getting Started  
To get started, you'll want to put all of your Linux targets in the 'inventory.txt' file. You'll also  
want to make sure that all of your Linux targets are either Debian-based forks, or RedHat-based forks.  
  
### Prerequisites  
On the machine you're running the playbook from, you'll want to make sure you have an ansible version newer  
than 2.4. It is also recommended that your target machines all have python 3 installed, however,  
python 2 will also work. To check your ansible version, you can run  
```sh
$ ansible --version
```  
  
### Installation  
1. Clone the repository
```sh
$ git clone https://gitlab.com/Omer.Tech/ansible-armor.git
```
2. Set file permissions  
```sh
$ chmod +x -R ./ansible-armor
```
3. Execute script
```sh
$ cd ansible-armor
$ ansible-playbook playbook.yml -i inventory.txt --become
```  
  
<!-- USAGE EXAMPLES -->
## Usage

I recommend running a 'ping' to all of your target machines before running this playbook. This will lower  
the chances of you having an SSH failure and having to re-run the playbook. You can do this by running:  
```sh
$ ansible targets -m ping
``` 
![](./.images/ansible_ping.gif)  
  
Once you have verified all of your machines can ping back to you, you should be ready to run the playbook.  
You can do this by running:  
```sh
$ ansible-playbook playbook.yml -i inventory.txt --become
```
![](./.images/ansible_playbook.gif)  

  
<!-- ROADMAP -->
## Roadmap  
See the [open issues](https://gitlab.com/Omer.Tech/ansible-armor/issues) for a list of proposed features (and known issues).
  
<!-- CONTRIBUTING -->
## Contributing  
Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **extremely appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request
  
<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.  



<!-- CONTACT -->
## Contact

Omer Turhan | [Email Me](mailto:omer.turhan@outlook.com) | [LinkedIn](https://linkedin.com/in/omer-turhan)

Project Link: [https://gitlab.com/Omer.Tech/ansible-armor](https://gitlab.com/Omer.Tech/ansible-armor)
  
<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Roshanlam/ReadMeTemplate](https://github.com/roshanlam/ReadMeTemplate/) 
* [geerlingguy/ansible-role-firewall](https://github.com/geerlingguy/ansible-role-firewall)
